# Real time chat app
Example of chat web application using Socket.IO with the MERN stack Client in React, Server in Node.js/Express 

Designed to save each conversation history in MongoDB with CRUD functionality on both the conversation history and on individual messages within that conversation.

# Set up
Clone repository in target path
$ git clone 
Install dependencies
$ node install

# Run in local development
npm start

# File Structure
**index.html** holds all the scripts, stylesheets and meta tags. It also displays the tab title
**API** this uses axios to create the connect for the app API
**Components-Pages/Chat.js** imports the API to use functions
<!-- May have this around the wrong way - may need to import the API into the Components instead  -->
**Routes.js** imports the chat component-page and provides the exact paths 
**App.js** imports the routes
**index.js** imports the App component
**Utils/socket.js** holds the socket connection
<!-- Not sure if this is correct - may need to have this in API instead  -->