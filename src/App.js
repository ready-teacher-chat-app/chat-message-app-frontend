import React from 'react';
import Routes from './Routes';
import './index.css';
import SelectUsername from './Components-Pages/SelectUsername/SelectUsername';
import Chat from './Components-Pages/Chat/Chat';
// import socket from './socket';

function App() {
	return (
		<div className='container fadein p-0'>
			<Routes />
		</div>
	);
}

export default App;
