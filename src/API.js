import axios from 'axios';
import { PORT } from './config';

const defaultOptions = {
	baseURL: PORT,
	timeout: 15000,
	headers: {
		'Content-Type': 'application/json',
	},
	// validateStatus: function (status) {
	// 	return status < 400; // Resolve only if the status code is less than 500
	// },
};

const axiosInstance = axios.create(defaultOptions);

axiosInstance.interceptors.request.use((config) => {
	const sessionID = localStorage.getItem("sessionID")
	// const token = localStorage.getItem("token")
	// config.headers.Authorization = token
	//   ? `Token ${window.localStorage.token}`
	//   : ""
	return config;
});

export default class coreAPI {
	constructor() {
		this._apiCore = axiosInstance;
	};

	// Add new api calls here

	getMessages() {
		return this._apiCore.get('/getMessages');
	};

	createMessage() {
		return this._apiCore.post('/createMessage', msg);
	};

	updateMessage() {
		return this._apiCore.put('/updatemessage', data);
	};

	deleteMessage() {
		return this._apiCore.delete('/deletemessage', data);
	};
};
