import './StatusIcon.css'

const StatusIcon = () => {

  return(
    <i className="icon"></i>
  )
}

export default StatusIcon;


// Vue code for reference:
// <i class="icon" :class="{ connected: connected }"></i>