import './User.css';
import StatusIcon from '../StatusIcon/StatusIcon';

const User = () => {
	return (
		<>
			<div className='user'>
				<div className='description'>
					<div className='name'></div>
					<div className='status'>
						<StatusIcon />
					</div>
				</div>
				<div className='new-messages'>!</div>
			</div>
		</>
	);
};
export default User;

// Vue code for reference
// <div class="user" @click="onClick" :class="{ selected: selected }">
// <div class="description">
//   <div class="name">
//     {{ user.username }} {{ user.self ? " (yourself)" : "" }}
//   </div>
//   <div class="status">
//     <status-icon :connected="user.connected" />{{ status }}
//   </div>
// </div>
// <div v-if="user.hasNewMessages" class="new-messages">!</div>
// </div>
