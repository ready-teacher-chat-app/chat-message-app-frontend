import './SelectUsername.css';

const SelectUsername = () => {
	return (
		<>
			<div className='select-username'>
				<form>
					<input placeholder='Your username...' />
					<button>Send</button>
				</form>
			</div>
		</>
	);
};

export default SelectUsername;

// Vue code for reference:
// <div class="select-username">
// <form @submit.prevent="onSubmit">
//   <input v-model="username" placeholder="Your username..." />
//   <button :disabled="!isValid">Send</button>
// </form>
// </div>
