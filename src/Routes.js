import React from 'react';
import { Route, Switch, BrowserRouter } from 'react-router-dom';
import Chat from './Components-Pages/Chat';

class Routes extends React.Component {
	render() {
		return (
			<BrowserRouter>
				<Switch>
					<Route exact path='/message' component={Chat} />
				</Switch>
			</BrowserRouter>
		);
	}
}

export default Routes;
